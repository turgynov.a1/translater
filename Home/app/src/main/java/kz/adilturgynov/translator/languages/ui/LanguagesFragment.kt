package kz.adilturgynov.translator.languages.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kz.adilturgynov.translator.R
import kz.adilturgynov.translator.base.BaseMvpFragment
import kz.adilturgynov.translator.languages.presenter.LanguagesPresenter
import kotlinx.android.synthetic.main.fragment_languages.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class LanguagesFragment : BaseMvpFragment<LanguagesContract.View, LanguagesContract.Presenter>(),
    LanguagesContract.View {

    companion object {
        fun create() = LanguagesFragment()
    }

    private val presenterImpl: LanguagesPresenter by viewModel()
    override val presenter: LanguagesContract.Presenter
        get() = presenterImpl

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View =
        inflater.inflate(R.layout.fragment_languages, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.loadLanguages()
        translatesButton.setOnClickListener {
            presenter.translates(LanguagesEditText.text.toString())
        }

    }

    override fun showFirstLanguages(text: String) {
        firstTranslationTextView.text = text
    }

    override fun showSecondLanguages(text: String) {
        secondTranslationTextView.text = text
    }

}