package kz.adilturgynov.translator

import android.os.Bundle
import kz.adilturgynov.translator.R
import kz.adilturgynov.translator.base.BaseActivity
import kz.adilturgynov.translator.translator.ui.TranslatorFragment


private const val PREF_KEY_CONVERTER_TEXT = "PREF_KEY_CONVERTER_TEXT"

class MainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        addFragment(TranslatorFragment.create())
    }
}
/*
    private var prefs: SharedPreferences? = null
    private var favAdapter: Adapter? = null


    //нужно для присвоения и сохранения переводов(звездочки)
    private var translatedText = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        prefs = PreferenceManager.getDefaultSharedPreferences(this)

        favAdapter = Adapter(
            avatarClickListener = { position: Int ->
                favAdapter?.removeItem(position)
            },
            favoriteSave = {
                translatedText += "$it"
            }
        )

        val concatenationList = mutableListOf<String>()


        button.setOnClickListener {
            CountWords(kyrEditText.text.toString())
            translate(kyrEditText.text.toString())

           // transText.text = translit(kyrEditText.text.toString())

            concatenationList.add("${kyrEditText.text} - ${latTextView.text}")
            setupWordList(concatenationList)
        }


    }

    private fun getSavedText(): String = prefs?.getString(PREF_KEY_CONVERTER_TEXT, "") ?: ""


    private fun translate(s: String): Unit {
        Observable
            .fromCallable {
                val kir: List<String> = listOf(
                    "А", "Ә", "Б", "В", "Г", "Ғ", "Д", "Е", "Ж", "З",
                    "И", "Й", "К", "Қ", "Л", "М", "Н", "Ң", "О", "Ө", "П",
                    "Р", "С", "Т", "У", "Ұ", "Ү", "Ф", "Х", "Һ", "Ч", "Ш", "Ы", "І",
                    "а", "ә", "б", "в", "г", "ғ", "д", "е", "ж", "з",
                    "и", "й", "к", "қ", "л", "м", "н", "ң", "о", "ө", "п",
                    "р", "с", "т", "у", "ұ", "ү", "ф", "х", "һ", "ч", "ш", "ы", "і"
                )
                val lat: List<String> = listOf(
                    "A", "Á", "B", "V", "G", "Ǵ", "D", "E", "J", "Z",
                    "I", "I", "K", "Q", "L", "M", "N", "Ń", "O", "Ó", "P",
                    "R", "S", "T", "Ý", "U", "Ú", "F", "H", "H", "Ch", "Sh", "Y", "I",
                    "a", "á", "b", "v", "g", "ǵ", "d", "e", "j", "z",
                    "ı", "ı", "k", "q", "l", "m", "n", "ń", "o", "ó", "p",
                    "r", "s", "t", "ý", "u", "ú", "f", "h", "h", "ch", "sh", "y", "i"
                )
                var StrLat = ""
                s.forEach {
                    if (kir.contains(it.toString())) {
                        StrLat += lat[kir.indexOf(it.toString())]
                    } else {
                        StrLat += it
                    }
                }
                StrLat
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    latTextView.text = it
                },
                {
                    Log.d("Error", it.message)
                }
            )

    }


    private fun CountWords(kyrText:String):Unit {
        Observable
            .fromCallable {
                var kyrRes:String = ""
                val kir: List<String> = listOf(
                    "А", "Ә", "Б", "В", "Г", "Ғ", "Д", "Е", "Ж", "З",
                    "И", "Й", "К", "Қ", "Л", "М", "Н", "Ң", "О", "Ө", "П",
                    "Р", "С", "Т", "У", "Ұ", "Ү", "Ф", "Х", "Һ", "Ч", "Ш", "Ы", "І",
                    "а", "ә", "б", "в", "г", "ғ", "д", "е", "ж", "з",
                    "и", "й", "к", "қ", "л", "м", "н", "ң", "о", "ө", "п",
                    "р", "с", "т", "у", "ұ", "ү", "ф", "х", "һ", "ч", "ш", "ы", "і"
                )
                kir.forEachIndexed{index: Int, s: String ->
                    var Counts = 0
                    kyrText.forEach { it:Char ->
                        if(s==it.toString()) {
                            Counts++
                        }
                    }
                    var res = Math.round(((Counts /kyrText.length.toDouble())*100))
                    if (Counts !=0) {
                        kyrRes += "${kir.get(index)} - $res % \n"
                    }
                }
                kyrRes
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    CountTextView.text = "$it"
                }, {
                    Log.d("Error", it.message)
                }
            )
    }

    fun setupWordList(list: MutableList<String>) {
        val manager = LinearLayoutManager(this)

        listRecyclerView.apply {
            layoutManager = manager
           adapter = favAdapter
        }
        favAdapter?.addWords(list)
    }


    fun translit(s: String): String {
        val kir: List<String> = listOf(
            "А", "Ә", "Б", "В", "Г", "Ғ", "Д", "Е", "Ж", "З",
            "И", "Й", "К", "Қ", "Л", "М", "Н", "Ң", "О", "Ө", "П",
            "Р", "С", "Т", "У", "Ұ", "Ү", "Ф", "Х", "Һ", "Ч", "Ш", "Ы", "І",
            "а", "ә", "б", "в", "г", "ғ", "д", "е", "ж", "з",
            "и", "й", "к", "қ", "л", "м", "н", "ң", "о", "ө", "п",
            "р", "с", "т", "у", "ұ", "ү", "ф", "х", "һ", "ч", "ш", "ы", "і"
        )
        val lat: List<String> = listOf(
            "A", "Á", "B", "V", "G", "Ǵ", "D", "E", "J", "Z",
            "I", "I", "K", "Q", "L", "M", "N", "Ń", "O", "Ó", "P",
            "R", "S", "T", "Ý", "U", "Ú", "F", "H", "H", "Ch", "Sh", "Y", "I",
            "a", "á", "b", "v", "g", "ǵ", "d", "e", "j", "z",
            "ı", "ı", "k", "q", "l", "m", "n", "ń", "o", "ó", "p",
            "r", "s", "t", "ý", "u", "ú", "f", "h", "h", "ch", "sh", "y", "i"
        )
        var StrLat: String = ""
        s.forEach {
            if (kir.contains(it.toString())) {
                StrLat += lat[kir.indexOf(it.toString())]
            } else {
                StrLat += it
            }
        }
        return StrLat
    }
*/

    /*
    private fun Count(kyrText:String):String{
        var kyrRes:String = ""
        val kir: List<String> = listOf(
            "А", "Ә", "Б", "В", "Г", "Ғ", "Д", "Е", "Ж", "З",
            "И", "Й", "К", "Қ", "Л", "М", "Н", "Ң", "О", "Ө", "П",
            "Р", "С", "Т", "У", "Ұ", "Ү", "Ф", "Х", "Һ", "Ч", "Ш", "Ы", "І",
            "а", "ә", "б", "в", "г", "ғ", "д", "е", "ж", "з",
            "и", "й", "к", "қ", "л", "м", "н", "ң", "о", "ө", "п",
            "р", "с", "т", "у", "ұ", "ү", "ф", "х", "һ", "ч", "ш", "ы", "і"
        )
        kir.forEachIndexed{index: Int, s: String ->
            var Counts = 0
            kyrText.forEach { it:Char ->
                if(s==it.toString()) {
                    Counts++
                }
            }
            var res = Math.round(((Counts /kyrText.length.toDouble())*100))
             if (Counts !=0) {
                 kyrRes += "${kir.get(index)} - $res % \n"
             }
        }
        return kyrRes
    }

    }
 */