package kz.adilturgynov.translator.languages.ui

import kz.adilturgynov.translator.base.MvpPresenter
import kz.adilturgynov.translator.base.MvpView

interface LanguagesContract {
    interface View : MvpView {
    fun showFirstLanguages(text: String)
    fun showSecondLanguages(text: String)
}

interface Presenter : MvpPresenter<View> {
    fun loadLanguages()
    fun translates(text: String)
}

}