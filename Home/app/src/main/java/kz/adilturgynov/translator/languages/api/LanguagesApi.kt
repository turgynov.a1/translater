package kz.adilturgynov.translator.languages.api

import kz.adilturgynov.translator.languages.model.Languages
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface LanguagesApi {
    companion object Factory {
        fun create(): LanguagesApi {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://simplechat2345.herokuapp.com/")
                .build()

            return retrofit.create(LanguagesApi::class.java);
        }
    }

    @GET("index.json")
    fun get(): Single<List<Languages>>
}