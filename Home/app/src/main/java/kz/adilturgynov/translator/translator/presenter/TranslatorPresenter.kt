package kz.adilturgynov.translator.translator.presenter

import android.util.Log
import kz.adilturgynov.translator.base.BasePresenter
import kz.adilturgynov.translator.translator.ui.TranslatorContract
import kz.adilturgynov.translator.translator.api.TranslatorApi
import kz.adilturgynov.translator.translator.interactor.translatorInteractor
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class TranslatorPresenter(
    private val translatorPrefApi: TranslatorApi,
    private val translatorInteractor: translatorInteractor
) : BasePresenter<TranslatorContract.View>(),
    TranslatorContract.Presenter {

    private val favouriteList: MutableList<String> = mutableListOf()
    private val concatenationList = mutableListOf<String>()

    override fun translate(s: String): Unit {
        Observable
            .fromCallable {
                val kir: List<String> = listOf(
                    "А", "Ә", "Б", "В", "Г", "Ғ", "Д", "Е", "Ж", "З",
                    "И", "Й", "К", "Қ", "Л", "М", "Н", "Ң", "О", "Ө", "П",
                    "Р", "С", "Т", "У", "Ұ", "Ү", "Ф", "Х", "Һ", "Ч", "Ш", "Ы", "І",
                    "а", "ә", "б", "в", "г", "ғ", "д", "е", "ж", "з",
                    "и", "й", "к", "қ", "л", "м", "н", "ң", "о", "ө", "п",
                    "р", "с", "т", "у", "ұ", "ү", "ф", "х", "һ", "ч", "ш", "ы", "і"
                )
                val lat: List<String> = listOf(
                    "A", "Á", "B", "V", "G", "Ǵ", "D", "E", "J", "Z",
                    "I", "I", "K", "Q", "L", "M", "N", "Ń", "O", "Ó", "P",
                    "R", "S", "T", "Ý", "U", "Ú", "F", "H", "H", "Ch", "Sh", "Y", "I",
                    "a", "á", "b", "v", "g", "ǵ", "d", "e", "j", "z",
                    "ı", "ı", "k", "q", "l", "m", "n", "ń", "o", "ó", "p",
                    "r", "s", "t", "ý", "u", "ú", "f", "h", "h", "ch", "sh", "y", "i"
                )
                var StrLat = ""
                s.forEach {
                    if (kir.contains(it.toString())) {
                        StrLat += lat[kir.indexOf(it.toString())]
                    } else {
                        StrLat += it
                    }
                }
                StrLat
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    Log.d("Lat", it)
                    view?.showLatTextView(it)
                    concatenationList.add(s + " - " + it)
                    view?.addToAdapter(concatenationList)

                },
                {
                    Log.d("Error", it.message)
                }
            )

    }


    override fun countWords(kyrText:String):Unit {
        Observable
            .fromCallable {
                var kyrRes:String = ""
                val kir: List<String> = listOf(
                    "А", "Ә", "Б", "В", "Г", "Ғ", "Д", "Е", "Ж", "З",
                    "И", "Й", "К", "Қ", "Л", "М", "Н", "Ң", "О", "Ө", "П",
                    "Р", "С", "Т", "У", "Ұ", "Ү", "Ф", "Х", "Һ", "Ч", "Ш", "Ы", "І",
                    "а", "ә", "б", "в", "г", "ғ", "д", "е", "ж", "з",
                    "и", "й", "к", "қ", "л", "м", "н", "ң", "о", "ө", "п",
                    "р", "с", "т", "у", "ұ", "ү", "ф", "х", "һ", "ч", "ш", "ы", "і"
                )
                kir.forEachIndexed{index: Int, s: String ->
                    var Counts = 0
                    kyrText.forEach { it:Char ->
                        if(s==it.toString()) {
                            Counts++
                        }
                    }
                    var res = Math.round(((Counts /kyrText.length.toDouble())*100))
                    if (Counts !=0) {
                        kyrRes += "${kir.get(index)} - $res % \n"
                    }
                }
                kyrRes
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    view?.showCountWords(it)
                }, {
                    Log.d("Error", it.message)
                }
            )
    }

    override fun loadFavourites() {
        translatorInteractor.getFavourites()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    favouriteList.clear()
                    favouriteList.addAll(it)
                    view?.addToAdapter(favouriteList)
                },
                {
                    Log.d("Error", it.message)
                }
            )
    }

    override fun getFavList(): MutableList<String> = favouriteList

    override fun saveToFavourites(word: String) {
        if(word.isNotEmpty()) {
            favouriteList.add(word)
            translatorPrefApi.saveFavouriteWords(favouriteList)
            }
    }

    override fun removeFromFavourites(pos: Int) {
        favouriteList.removeAt(pos)
        translatorPrefApi.saveFavouriteWords(favouriteList)
    }


}