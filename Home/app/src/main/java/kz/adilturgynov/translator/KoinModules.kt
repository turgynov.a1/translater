package kz.adilturgynov.translator

import kz.adilturgynov.translator.languages.LanguagesModule
import kz.adilturgynov.translator.translator.TranslatorModule
import org.koin.core.module.Module

object KoinModules {
    val modules: List<Module> =
        listOf(
            TranslatorModule.create(),
            LanguagesModule.create()
        )
}