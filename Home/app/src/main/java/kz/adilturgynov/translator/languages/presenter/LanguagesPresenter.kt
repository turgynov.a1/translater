package kz.adilturgynov.translator.languages.presenter

import android.util.Log
import kz.adilturgynov.translator.base.BasePresenter
import kz.adilturgynov.translator.languages.api.LanguagesApi
import kz.adilturgynov.translator.languages.interactor.LanguagesInteractor
import kz.adilturgynov.translator.languages.model.Languages
import kz.adilturgynov.translator.languages.ui.LanguagesContract
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class LanguagesPresenter (
    private val LanguagesPrefApi: LanguagesApi,
    private val LanguagesInteractor: LanguagesInteractor
) : BasePresenter<LanguagesContract.View>(),
    LanguagesContract.Presenter {

    private val LanguagesList: MutableList<Languages> = mutableListOf()

    override fun loadLanguages() {
        LanguagesInteractor.getLanguages()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    LanguagesList.addAll(it)
                    Log.d("language", it.toString())
                },
                {
                    Log.d("Error", it.message)
                }
            )
    }

    override fun translates(text: String) {
        LanguagesList.forEach{
            if(text == it.ru) {
                view?.showFirstLanguages("Английский: " + it.en)
                view?.showSecondLanguages("Казахский: " + it.kz)
            }
            if (text == it.en){
                view?.showFirstLanguages("Русский: " + it.ru)
                view?.showSecondLanguages("Казахский: " + it.kz)
            }
            if (text == it.kz){
                view?.showFirstLanguages("Английский: " + it.en)
                view?.showSecondLanguages("Русский: " + it.ru)
            }

        }
    }

}