package kz.adilturgynov.translator.translator

import androidx.preference.PreferenceManager
import kz.adilturgynov.translator.base.InjectionModule
import kz.adilturgynov.translator.translator.api.TranslatorApi
import kz.adilturgynov.translator.translator.interactor.translatorInteractor
import kz.adilturgynov.translator.translator.presenter.TranslatorPresenter
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object TranslatorModule : InjectionModule {
    override fun create() = module {
        single { TranslatorApi(PreferenceManager.getDefaultSharedPreferences(get())) }
        single { translatorInteractor(get()) }
        viewModel { TranslatorPresenter(get(), get()) }
    }
}