package kz.adilturgynov.translator.translator.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import kz.adilturgynov.translator.Adapter
import kz.adilturgynov.translator.R
import kz.adilturgynov.translator.base.BaseMvpFragment
import kz.adilturgynov.translator.languages.ui.LanguagesFragment
import kz.adilturgynov.translator.translator.presenter.TranslatorPresenter
import kotlinx.android.synthetic.main.fragment_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class TranslatorFragment : BaseMvpFragment<TranslatorContract.View, TranslatorContract.Presenter>(),
    TranslatorContract.View {

    private var favAdapter: Adapter? = null


    companion object {
        fun create() = TranslatorFragment()
    }

    private val presenterImpl: TranslatorPresenter by viewModel()
    override val presenter: TranslatorContract.Presenter
        get() = presenterImpl

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View =
        inflater.inflate(R.layout.fragment_main, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        presenter.loadFavourites()
        this.createAdapter()
        button.setOnClickListener {
            presenter.translate(kyrEditText.text.toString())
            presenter.countWords(kyrEditText.text.toString())
        }
        languagesbutton.setOnClickListener {
            fragmentManager
                ?.beginTransaction()
                ?.replace(R.id.container, LanguagesFragment.create())
                ?.apply { addToBackStack(this::class.java.simpleName) }
                ?.commit()
        }



    }

    override fun addToAdapter(list: List<String>) {
        favAdapter?.addWords(list)
    }

    private fun createAdapter() {
        favAdapter = Adapter(
            avatarClickListener = { position: Int ->
                favAdapter?.removeItem(position)
                presenter.removeFromFavourites(position)
            },
            favoriteSave = {
                presenter.saveToFavourites(it)
            }
        )
        val manager = LinearLayoutManager(context)
        listRecyclerView.apply {
            layoutManager = manager
            adapter = favAdapter
        }
    }

    override fun showLatTextView(text: String) {
        latTextView.text = text
    }

    override fun showCountWords(kyrText: String) {
        CountTextView.text = kyrText
    }


}
