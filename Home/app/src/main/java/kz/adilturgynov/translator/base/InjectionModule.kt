package kz.adilturgynov.translator.base

import org.koin.core.module.Module

interface InjectionModule {
    fun create(): Module
}