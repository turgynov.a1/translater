package kz.adilturgynov.translator.translator.api

import android.content.SharedPreferences
import io.reactivex.Single

private const val FAVOURITES_PREF = "FAVOURITES_PREF"

class TranslatorApi (private val prefs: SharedPreferences)  {

    fun saveFavouriteWords(list:MutableList<String>){
        val editor = prefs.edit()
        editor?.putStringSet(FAVOURITES_PREF, list.toSet())
        editor?.apply()
    }

    fun getFavouriteWords(): Single<MutableList<String>> =
        Single.fromCallable {
            prefs.getStringSet(FAVOURITES_PREF, mutableSetOf(""))?.toMutableList()
        }
}