package kz.adilturgynov.translator

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kz.adilturgynov.translator.R
import kotlinx.android.synthetic.main.item.view.*

class Adapter(
    private val avatarClickListener:(position: Int) -> Unit,
    private val favoriteSave:(translateText:String) -> Unit

): RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    private val OriginalandTranslatesList = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return UserViewHolder(inflater, parent)

    }

    override fun getItemCount(): Int = OriginalandTranslatesList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as UserViewHolder).bind(
            OriginalandTranslatesList[position],
            avatarClickListener,
            favoriteSave)

    }

    fun addWords(list: List<String>) {
        OriginalandTranslatesList.clear()
        OriginalandTranslatesList.addAll(list)
        notifyDataSetChanged()
    }


    fun removeItem(position: Int) {
        if (OriginalandTranslatesList.size > 0) {
            OriginalandTranslatesList.removeAt(position)
            notifyDataSetChanged()
        }
    }


    private class UserViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.item, parent, false)) {

        private val TranslatesTextView = itemView.translatesTextView
        private val trashBinImageView = itemView.binImageView
        private val starImageView = itemView.starImageView

        fun bind(
            word: String,
            avatarClickListener: (position: Int) -> Unit,
            favoriteSave: (translateText: String) -> Unit
        ) {
            TranslatesTextView.text = word
            trashBinImageView.setOnClickListener {
                avatarClickListener(adapterPosition)
            }
            starImageView.setOnClickListener {
                starImageView.setColorFilter(Color.BLUE)
                favoriteSave(word)
            }
        }
    }
}