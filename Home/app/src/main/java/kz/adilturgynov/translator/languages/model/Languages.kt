package kz.adilturgynov.translator.languages.model

data class Languages (
    val kz: String,
    val ru: String,
    val en: String
) {
    override fun toString(): String {
        return "$kz - $ru - $en \n"
    }
}
