package kz.adilturgynov.translator.languages.interactor

import kz.adilturgynov.translator.languages.api.LanguagesApi
import kz.adilturgynov.translator.languages.model.Languages
import io.reactivex.Single

class LanguagesInteractor (private val languagesApi: LanguagesApi) {
    fun getLanguages(): Single<List<Languages>> = languagesApi.get()
}