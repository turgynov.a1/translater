package kz.adilturgynov.translator.languages

import kz.adilturgynov.translator.base.InjectionModule
import kz.adilturgynov.translator.languages.api.LanguagesApi
import kz.adilturgynov.translator.languages.interactor.LanguagesInteractor
import kz.adilturgynov.translator.languages.presenter.LanguagesPresenter
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object LanguagesModule : InjectionModule {
    override fun create() = module {
        single { LanguagesApi.create() }
        single { LanguagesInteractor(get()) }
        viewModel { LanguagesPresenter(get(), get()) }
    }
}