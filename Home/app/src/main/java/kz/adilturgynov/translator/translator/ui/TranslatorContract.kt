package kz.adilturgynov.translator.translator.ui

import kz.adilturgynov.translator.base.MvpPresenter
import kz.adilturgynov.translator.base.MvpView

interface TranslatorContract {
    interface View : MvpView {
        fun showLatTextView(text: String)
        fun showCountWords(kyrText: String)
        fun addToAdapter(list:List<String>)
    }

    interface Presenter : MvpPresenter<View> {
        fun loadFavourites()
        fun translate(s: String): Unit
        fun getFavList():MutableList<String>
        fun saveToFavourites(word:String)
        fun removeFromFavourites(pos:Int)
        fun countWords(kyrText:String):Unit
    }
}