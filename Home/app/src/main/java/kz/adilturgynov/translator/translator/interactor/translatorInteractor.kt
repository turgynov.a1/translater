package kz.adilturgynov.translator.translator.interactor

import kz.adilturgynov.translator.translator.api.TranslatorApi
import io.reactivex.Single

class translatorInteractor(private val translatorApi: TranslatorApi) {
    fun getFavourites():Single<MutableList<String>> = translatorApi.getFavouriteWords()
}