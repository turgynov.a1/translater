package kz.adilturgynov.translator.base

interface MvpPresenter<V : MvpView> {

    fun attach(view: V)

    fun detach()
}